﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Moyashi.Source;
using NUnit.Framework;
using TargetCode.Source;

namespace Moyashi.Tests
{
    [TestFixture]
    public class MoyashiShould
    {
        [Test]
        public void ProveThatRunsTheTests()
        {
            Assert.Pass("Tests are running");
        }

        [Test]
        public async Task FindATestReferencingTheMethod()
        {
            var mutator = new Mutator();
            var testMethods = new[]
            {
                "SuperMindShould.ShowWelcomeMessageWhenStarts"
            };

            var foundTests = await mutator
                                .FindReferencesForAsync(nameof(SuperMind.Start));

            foundTests.Should().BeEquivalentTo(testMethods);
        }
    }
}
