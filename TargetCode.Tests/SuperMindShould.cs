﻿using NSubstitute;
using NUnit.Framework;
using TargetCode.Source;

namespace TargetCode.Tests
{
    [TestFixture]
    public class SuperMindShould
    {
        [Test]
        public void ShowWelcomeMessageWhenStarts()
        {
            var console = Substitute.For<IConsole>();
            var superMind = new SuperMind(console);

            superMind.Start();

            console.Received().Show("Welcome to SuperMind");
        }
    }
}