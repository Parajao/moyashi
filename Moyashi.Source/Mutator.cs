﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.FindSymbols;
using Microsoft.CodeAnalysis.MSBuild;
using Microsoft.CodeAnalysis.CSharp;

namespace Moyashi.Source
{
    public class Mutator
    {
        public async Task<IReadOnlyCollection<string>> FindReferencesForAsync(string methodName)
        {
            try
            {
                var SOLUTION_PATH = @"C:\Users\Parajao\Documents\Visual Studio 2015\Projects\Moyashi\Moyashi.Source.sln";
                var CLASS_FILE_NAME = "SuperMind.cs";
                var PROJECT_NAME = "TargetCode.Source";

                var solution = await MSBuildWorkspace
                    .Create()
                    .OpenSolutionAsync(SOLUTION_PATH)
                    .ConfigureAwait(false);

                var document = solution.Projects
                    .First(p => p.Name == PROJECT_NAME)
                    .Documents.Single(d => d.Name == CLASS_FILE_NAME);

                var root = await document.GetSyntaxRootAsync();

                var methodsDeclarations = root.DescendantNodes()
                    .OfType<MethodDeclarationSyntax>();

                var methodDeclaration = methodsDeclarations
                                        .First(md => md.Identifier.Text.Contains(methodName));

                var model = await document.GetSemanticModelAsync().ConfigureAwait(false);
                var methodSymbol = model.GetDeclaredSymbol(methodDeclaration);
                var referencesToMethod = SymbolFinder.FindReferencesAsync(methodSymbol, solution).Result;

                return (from r in referencesToMethod
                        from l in r.Locations
                        select l.ToString())
                       .ToImmutableArray();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}