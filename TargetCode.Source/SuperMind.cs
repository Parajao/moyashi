﻿namespace TargetCode.Source
{
    public class SuperMind
    {
        private readonly IConsole _console;

        public SuperMind(IConsole console)
        {
            _console = console;
        }

        public void Start()
        {
            _console.Show("Welcome to SuperMind");
        }
    }

    public interface IConsole
    {
        void Show(string message);
    }
}
