#MOYASHI
A .Net mutation testing tool based on Roslyn.

##What is mutation testing?
Mutation testing is a technique to improve the quality of a test harness.

##How does it work?
It creates variations, called mutations of your code and runs your tests against them.
If your tests don't fail it means that the changed code is not really exercised hence you should improve your tests quality.

##Prerequisites
1. Install Roslyn through package manager console: Install-Package Microsoft.CodeAnalysis

##Resources
###Learn Roslyn
https://joshvarty.wordpress.com/learn-roslyn-now/

###Learn Visual Studio Extensibility
https://learnvsxnow.codeplex.com/

###Fixing Visual Studio 2015 Update 3
http://vimvq1987.com/2016/07/fixing-visual-studio-2015-update-3/

###Nuget Console
https://dist.nuget.org/win-x86-commandline/latest/nuget.exe